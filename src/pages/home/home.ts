import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation'; //importando Geolocalização

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lat: any; //Cria variável lat que é igual á Latitude
  lon: any; //Cria variável lon que é igual á Longitude
  pos: any;
  // Importando Geolocalização para o constructor
  constructor(public navCtrl: NavController, public geo: Geolocation) {
  }

  //Vai ser chamado assim que entrar na HomePage
  ionViewDidLoad() {
    //Pega a Posição atual puxa as coordenadas e gera latitude e longitude
    this.geo.getCurrentPosition().then(pos => {
      this.lat = pos.coords.latitude;
      this.lon = pos.coords.longitude;
    }).catch(err => console.log(err));   //caso haja algum erro, irá mostrar no console
  }
}
